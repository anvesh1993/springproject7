package com.example.RetryRecoverable;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.retry.annotation.EnableRetry;

@SpringBootApplication
@EnableRetry
public class RetryRecoverableApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetryRecoverableApplication.class, args);
	}

}
